<div id="sidebar"> <a href="#" class="visible-phone"><i class="icon icon-th"></i>Menu</a>
  <ul>
    <li><a href="<?php echo site_url('home/index');?>"><i class="icon icon-home"></i> <span>Home</span></a> </li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Matakuliah</span></a>
      <ul>
        <li><a href="<?php echo site_url('matakuliah/index');?>"><i class="icon icon-pencil"></i>&nbsp;Matakuliah</a></li>
        <li><a href="<?php echo site_url('jadwal/index');?>"><i class="icon icon-calendar"></i>&nbsp;Jadwal</a></li>
        <li><a href="<?php echo site_url('nilai/index');?>"><i class="icon icon-table"></i>&nbsp;Nilai</a></li>
      </ul>
    </li>
    <li><a href="<?php echo site_url('home/mahasiswa');?>"><i class="icon icon-group"></i><span>Mahasiswa</span> </a></li>
    <li><a href="<?php echo site_url('dos/dosen/viewDos');?>"><i class="icon icon-user-md"></i><span>Dosen</span></a></li>
    <li><a href="<?php echo site_url('jur/jurusan/index');?>"><i class="icon icon-book"></i><span>Jurusan</span></a></li>
  </ul>
</div>
