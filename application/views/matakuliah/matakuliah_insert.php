</style>
<div class="container">
  <div class="row-fluid">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Tambah Matakuliah</h5>
          </div>
          <div class="widget-content nopadding">
            <form action="<?php echo site_url('matakuliah/saveMatakuliah');?>" method="POST" class="form-horizontal" name="basic_validate" id="basic_validate" novalidate="novalidate" >
                <div class="control-group">
                  <label class="control-label">Kode Matakuliah :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="komatkul" placeholder="Kode Matakuliah" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Matakuliah :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="matkul" placeholder="Matakuliah" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">SKS :</label>
                  <div class="controls">
                    <input type="text" class="span2" name="sks" placeholder="SKS" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Semester :</label>
                  <div class="controls">
                    <input type="text" class="span4" name="sems" placeholder="Semester" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" id="coba">Jurusan :</label>
                  <div class="controls">
                    <select class="span7" name="jur">
                      <option value="">--Pilih Jurusan--</option>
                      <?php foreach ($list_jur as $row) { ?>
                            <option value ="<?php echo $row->kode_jur; ?>"><?php echo $row ->nama_jurusan; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Deskripsi</label>
                  <div class="controls">
                    <textarea class="span7" name="desc"></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Referensi</label>
                  <div class="controls">
                    <textarea class="span7" name="ref"></textarea>
                  </div>
                </div>
              <div class="form-actions" id="pesan">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>
