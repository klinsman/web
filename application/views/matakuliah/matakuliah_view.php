<h3>Matakuliah</h3>
    <hr>
    <div class="">
      <a href="<?php echo site_url('matakuliah/tambahMatakuliah'); ?>" class="btn btn-primary btn-large"><i class="icon icon-plus"></i> Add</a>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-group"></i></span>
            <h5>Matakuliah</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Matakuliaa</th>
                  <th>Matakuliah</th>
                  <th>SKS</th>
                  <th>Semester</th>
                  <th>Jurusan</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach ($listMatakuliah as $row): ?>
                  <tr class="gradeX">
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row->kode_matkul; ?></td>
                    <td><?php echo $row->nama_matkul; ?></td>
                    <td><?php echo $row->sks; ?></td>
                    <td><?php echo $row->semester; ?></td>
                    <td><?php echo $row->nama_jurusan; ?></td>
                    <td class="center">
                      <a href="<?php echo site_url('matakuliah/edit/'.$row->kode_matkul)?>" class="btn btn-info"><i class="icon icon-pencil"></i></a>
                      <a href="<?php echo site_url('matakuliah/delMatakuliah/'.$row->kode_matkul)?>" onclick="return confirm('Apakah anda ingin menghapus data ini?')" class="btn btn-danger"><i class="icon icon-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
