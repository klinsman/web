</style>
<div class="container">
  <div class="row-fluid">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Tambah Matakuliah</h5>
          </div>
          <div class="widget-content nopadding">
            <form action="<?php echo site_url('matakuliah/upMatakuliah')?>" method="POST" class="form-horizontal" name="basic_validate" id="basic_validate" novalidate="novalidate" >
                <?php $row = current($listMatakuliah) ?>
                <div class="control-group">
                  <label class="control-label">Kode Matakuliah :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="komatkul" value="<?php echo $row->kode_matkul;?>" readonly />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Matakuliah :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="matkul" value="<?php echo $row->nama_matkul;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">SKS :</label>
                  <div class="controls">
                    <input type="text" class="span2" name="sks" value="<?php echo $row->sks;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Semester :</label>
                  <div class="controls">
                    <input type="text" class="span4" name="sems" value="<?php echo $row->semester;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" id="coba">Jurusan :</label>
                  <div class="controls">
                    <select class="span7" name="jur">
                      <option value="">--Pilih Jurusan--</option>
                      <?php foreach ($list_jur as $res) { ?>
                            <option value ="<?php echo $res->kode_jur;?>" <?php echo $row->kode_jur == $res->kode_jur ? 'selected' : ''; ?>><?php echo $res ->nama_jurusan; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Deskripsi</label>
                  <div class="controls">
                    <textarea class="span7" name="desc"><?php echo $row->description; ?></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Referensi</label>
                  <div class="controls">
                    <textarea class="span7" name="ref"><?php echo $row->referensi; ?></textarea>
                  </div>
                </div>
              <div class="form-actions" id="pesan">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>
