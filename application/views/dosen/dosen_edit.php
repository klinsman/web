<div class="container">
  <div class="row-fluid">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Tambah Dosen</h5>
          </div>
          <div class="widget-content nopadding">
            <form action="<?php echo site_url('dos/dosen/edit'); ?>" method="POST" class="form-horizontal" name="basic_validate" id="basic_validate" novalidate="novalidate" >
              <?php $row = current($get_Dos) ?>
              <div class="control-group">
                <label class="control-label">NIDN :</label>
                <div class="controls">
                  <input type="text" class="span11" name="nidn" readonly value="<?php echo $row->NIDN; ?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Nama Dosen :</label>
                <div class="controls">
                  <input type="text" class="span11" name="nDos" value="<?php echo $row->nama_dos?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Jenis Kelamin :</label>
                <div class="controls">
                  <input type="radio" class="span11" name="jk" value="L" <?php echo set_value('jk', $row->j_kel) == 'L' ?"checked" : ""; ?>/>Laki-laki
                  <input type="radio" class="span11" name="jk" value="P" <?php echo set_value('jk', $row->j_kel) == 'P' ?"checked" : ""; ?>/>Perempuan
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Jurusan :</label>
                <div class="controls">
                  <input type="text" class="span11" name="jur" value="<?php echo $row->jurusan; ?>" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Pendidikan :</label>
                <div class="controls">
                  <input type="text" class="span11" name="pen" value="<?php echo $row->pendidikan; ?>" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Telpon/HP :</label>
                <div class="controls">
                  <input type="text" class="span11" name="telp" value="<?php echo $row->telepon; ?>" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Email :</label>
                <div class="controls">
                  <input type="text" class="span11" name="email" id="email" value="<?php echo $row->email; ?>" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Alamat</label>
                <div class="controls">
                  <textarea class="span11" name="alm"><?php echo $row->alamat; ?></textarea>
                </div>
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>
