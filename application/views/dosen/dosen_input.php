<div class="container">
  <div class="row-fluid">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Tambah Dosen</h5>
          </div>
          <div class="widget-content nopadding">
            <form action="<?php echo site_url('dos/dosen/saveDos')?>" method="POST" class="form-horizontal" name="basic_validate" id="basic_validate" novalidate="novalidate" >
              <div class="control-group">
                <label class="control-label">NIDN :</label>
                <div class="controls">
                  <input type="text" class="span11" name="nidn" placeholder="NIDN" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Nama Dosen :</label>
                <div class="controls">
                  <input type="text" class="span11" name="nDos" placeholder="Nama dosen" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Jenis Kelamin :</label>
                <div class="controls">
                  <input type="radio" class="span11" name="jk" value="L" />Laki-laki
                  <input type="radio" class="span11" name="jk" value="P" />Perempuan
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Jurusan :</label>
                <div class="controls">
                  <input type="text" class="span11" name="jur" placeholder="Jurusan" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Pendidikan :</label>
                <div class="controls">
                  <select class="span11" name="pen">
                    <option value="Sarjana">Sarjana</option>
                    <option value="Master">Master</option>
                    <option value="Doctor">Doctor</option>
                  </select>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Telpon/HP :</label>
                <div class="controls">
                  <input type="text" class="span11" name="telp" placeholder="Telepon" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Email :</label>
                <div class="controls">
                  <input type="text" class="span11" name="email" id="email" placeholder="Example@email.com" />
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Alamat</label>
                <div class="controls">
                  <textarea class="span11" name="alm"></textarea>
                </div>
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>
