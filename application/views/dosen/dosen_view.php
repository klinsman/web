<h3>Dosen</h3>
    <hr>
    <div class="">
      <a href="<?php echo site_url('dos/dosen/index'); ?>" class="btn btn-primary btn-large"><i class="icon icon-plus"></i> Add</a>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-user-md"></i></span>
            <h5>Dosen</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>NIDN</th>
                  <th>Nama Dosen</th>
                  <th>Jenis Kelamin</th>
                  <th>Jurusan</th>
                  <th>Pendidikan</th>
                  <th>Telepon</th>
                  <th>Email</th>
                  <th>Alamat</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach ($list_dos as $row):
                  $gender=$row->j_kel=='L' ? 'Laki-laki' : 'Perempuan';
                  ?>
                  <tr class="gradeX">
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row->NIDN; ?></td>
                    <td><?php echo $row->nama_dos; ?></td>
                    <td><?php echo $gender; ?></td>
                    <td><?php echo $row->jurusan; ?></td>
                    <td><?php echo $row->pendidikan; ?></td>
                    <td><?php echo $row->telepon; ?></td>
                    <td><?php echo $row->email; ?></td>
                    <td><?php echo $row->alamat; ?></td>
                    <td class="center">
                      <a href="<?php echo site_url('dos/dosen/editDos/'.$row->NIDN);?>" class="btn btn-info"><i class="icon icon-edit"></i></a>
                      <a href="<?php echo site_url('dos/dosen/delDos/'.$row->NIDN);?>" onclick="return confirm('Apakah anda ingin menghapus data ini?')" class="btn btn-danger"><i class="icon icon-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
<script type="text/javascript">

</script>
