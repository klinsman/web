</style>
<div class="container">
  <div class="row-fluid">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Tambah Mahasiswa</h5>
          </div>
          <div class="widget-content nopadding">
            <div class="form-horizontal">
            <?php echo form_open('mahasiswa/updateMahasiswa'); ?>
              <?php $row = current($listMahasiswa) ?>
                <div class="control-group">
                  <label class="control-label">NIM :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="nim" readonly value="<?php echo $row->NIK;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Nama Lengkap :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="nama" value="<?php echo $row->nama_sis;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Tempat Lahir :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="tm_lahir" value="<?php echo $row->tem_lahir;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Tanggal Lahir :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="tg_lahir" value="<?php echo $row->tgl_lahir;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Jenis Kelamin :</label>
                  <div class="controls">
                    <input type="radio" class="span7" name="jk" value="L" <?php echo set_value('jk', $row->j_kel)== 'L' ? "checked":""; ?>/>Laki-laki
                    <input type="radio" class="span7" name="jk" value="P" <?php echo set_value('jk', $row->j_kel)== 'P' ? "checked":""; ?>/>Perempuan
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Jurusan :</label>
                  <div class="controls">
                    <select class="span7" id="jur" name="jur">
                      <option value="">--Pilih Jurusan--</option>
                      <?php foreach ($list_jur as $res) { ?>
                            <option value ="<?php echo $res->kode_jur;?>" <?php echo $row->kode_jur == $res->kode_jur ? 'selected' : ''; ?>><?php echo $res ->nama_jurusan; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <?php $row = current($listMahasiswa) ?>
                  <label class="control-label">Agama :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="agama" value="<?php echo $row->agama;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Telpon/HP :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="telp" value="<?php echo $row->telpon;?>" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Alamat</label>
                  <div class="controls">
                    <textarea class="span7" name="alm"><?php echo $row->alamat_sis; ?></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Email :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="email" id="email" value="<?php echo $row->email;?>" />
                  </div>
                </div>
              <div class="form-actions" id="pesan">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            <?php echo form_close(); ?>
          </div>
        </div>
      </div>
  </div>
</div>
</div>
