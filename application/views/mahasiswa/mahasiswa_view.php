<h3>Mahasiswa</h3>
    <hr>
    <div class="">
      <a href="<?php echo site_url('mahasiswa/index'); ?>" class="btn btn-primary btn-large"><i class="icon icon-plus"></i> Add</a>
    </div>
    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"><i class="icon-group"></i></span>
            <h5>Mahasiswa</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIM</th>
                  <th>Nama Mahasiswa</th>
                  <th>Jenis Kelamin</th>
                  <th>Jurusan</th>
                  <th>Alamat</th>
                  <th>Telpon</th>
                  <th>Email</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach ($listMahasiswa as $row):
                  $gender = $row->j_kel=='L'? 'Laki-laki' : 'Perempuan';
                  ?>
                  <tr class="gradeX">
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $row->NIK; ?></td>
                    <td><?php echo $row->nama_sis; ?></td>
                    <td><?php echo $gender; ?></td>
                    <td><?php echo $row->nama_jurusan; ?></td>
                    <td><?php echo $row->alamat_sis; ?></td>
                    <td><?php echo $row->telpon; ?></td>
                    <td><?php echo $row->email; ?></td>
                    <td class="center">
                      <a href="<?php echo site_url('mahasiswa/edit/'.$row->NIK);?>" class="btn btn-info"><i class="icon icon-pencil"></i></a>
                      <a href="<?php echo site_url('mahasiswa/delMahasiswa/'.$row->NIK)?>" onclick="return confirm('Apakah anda ingin menghapus data ini?')" class="btn btn-danger"><i class="icon icon-trash"></i></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
