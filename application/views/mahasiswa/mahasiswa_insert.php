<div class="container">
  <div class="row-fluid">
      <div class="span9">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Tambah Mahasiswa</h5>
          </div>
          <div class="widget-content nopadding">
            <form action="<?php echo site_url('mahasiswa/saveMahasiswa');?>" method="POST" class="form-horizontal" name="basic_validate" id="basic_validate" novalidate="novalidate" >
                <div class="control-group">
                  <label class="control-label">NIM :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="nim" placeholder="NIM" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Nama Lengkap :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="nama" placeholder="Nama lengkap" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Tempat Lahir :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="tm_lahir" placeholder="Tempat Lahir" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Tanggal Lahir :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="tg_lahir" placeholder="YYYY-MM-DD" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Jenis Kelamin :</label>
                  <div class="controls">
                    <input type="radio" class="span7" name="jk" value="L" />Laki-laki
                    <input type="radio" class="span7" name="jk" value="P" />Perempuan
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" id="coba">Jurusan :</label>
                  <div class="controls">
                    <select class="span7" name="jur">
                      <option value="">--Pilih Jurusan--</option>
                      <?php foreach ($list_jur as $row) { ?>
                            <option value ="<?php echo $row->kode_jur; ?>"><?php echo $row ->nama_jurusan; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Agama :</label>
                  <div class="controls">
                    <select class="span7" name="agama">
                      <option value="">--Pilih Agama--</option>
                      <option value="Islam">Islam</option>
                      <option value="Kristen Protestan">Kristen Protestan</option>
                      <option value="Kristen Katolik">Kristen Katolik</option>
                      <option value="Hindu">Hindu</option>
                      <option value="Buddha">Buddha</option>
                    </select>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Telpon/HP :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="telp" placeholder="Telepon" />
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Alamat</label>
                  <div class="controls">
                    <textarea class="span7" name="alm"></textarea>
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label">Email :</label>
                  <div class="controls">
                    <input type="text" class="span7" name="email" id="email" placeholder="Example@email.com" />
                  </div>
                </div>
              <div class="form-actions" id="pesan">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>
