<div class="container">
  <div class="row-fluid">
      <div class="span8">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
            <h5>Tambah Jurusan</h5>
          </div>
          <div class="widget-content nopadding">
            <form action="<?php echo site_url('jur/jurusan/edit') ;?>" method="POST" class="form-horizontal">
              <?php $row = current($list_jur) ?>
              <div class="control-group">
                <label class="control-label">Kode Jurusan :</label>
                <div class="controls">
                  <input type="text" class="span11" name="kJur" readonly value="<?php echo $row->kode_jur;?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Nama Jurusan :</label>
                <div class="controls">
                  <input type="text" class="span11" name="nJur" value="<?php echo $row->nama_jurusan;?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Kaprodi :</label>
                <div class="controls">
                  <input type="text" class="span11" name="kaprodi" value="<?php echo $row->kaprodi ?>"/>
                </div>
              </div>
              <div class="control-group">
                <label class="control-label">Deskripsi</label>
                <div class="controls">
                  <textarea class="span11" name="desk"><?php echo $row->deskrip; ?></textarea>
                </div>
              </div>
              <div class="form-actions">
                <button type="submit" class="btn btn-success">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  </div>
</div>
