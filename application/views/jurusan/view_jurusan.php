<h3>Jurusan</h3>
<hr>
<div class="">
  <a href="<?php echo site_url('jur/jurusan/jur'); ?>" class="btn btn-primary btn-large"><i class="icon icon-plus"></i> Add</a>
</div>
<div class="widget-box">
  <div class="widget-content nopadding">
    <table class="table table-bordered table-striped table-responsive">
      <thead>
        <tr>
          <th>No</th>
          <th>Kode Jurusan</th>
          <th>Nama Jurusan</th>
          <th>Kaprodi</th>
          <th>Deskripsi</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; foreach ($list_jur as $row): ?>
        <tr>
          <td><?php echo $no++; ?></td>
          <td><?php echo $row->kode_jur; ?></td>
          <td><?php echo $row->nama_jurusan; ?></td>
          <td><?php echo $row->kaprodi; ?></td>
          <td><?php echo $row->deskrip; ?></td>
          <td>
            <a href="<?php echo site_url('jur/jurusan/editJur/'.$row->kode_jur);?>" class="btn btn-info"><i class="icon icon-pencil"></i></a>
            <a href="<?php echo site_url('jur/jurusan/delJur/'.$row->kode_jur);?>" onclick="return confirm('Apakah anda ingin menghapus data ini?')" class="btn btn-danger"><i class="icon icon-trash"></i></a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
