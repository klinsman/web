<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matakuliah_model extends CI_Model {

  public $variable;

  public function __construct()
  {
    parent::__construct();

  }
public function saveMatakuliah($value='')
{
  $data = array(
    'kode_matkul' => $this->input->post('komatkul') ,
    'nama_matkul' => $this->input->post('matkul'),
    'sks'         => $this->input->post('sks'),
    'semester'    => $this->input->post('sems'),
    'kode_jur'    => $this->input->post('jur'),
    'description' => $this->input->post('desc'),
    'referensi'   => $this->input->post('ref')

  );
  $this->db->insert('tb_matakuliah',$data);

  if (condition==TRUE) {
    redirect('matakuliah/tambahMatakuliah');
  }
}
function tambahJur()
  {
    $this->db->select('*')
          ->from('tb_jurusan');
    $result=array();
    $query=$this->db->get();

    if ($query->num_rows() > 0) {
      $result= $query->result();
    }
    return $result;
  }

public function getMatakuliah()
{
  $this->db->select('k.kode_matkul,k.nama_matkul,k.sks,k.semester,j.nama_jurusan', FALSE)
            ->from('tb_matakuliah k')
            ->join('tb_jurusan j', 'j.kode_jur=k.kode_jur');
  $result=array();
  $query=$this->db->get();

  if ($query->num_rows() > 0) {
    $result= $query->result();
  }
  return $result;
}
public function getMatakuliah1($kodmatkul)
{
  $this->db->select('*')
        ->from('tb_matakuliah')
        ->where('kode_matkul',$kodmatkul);

  $row = $this->db->get();
  $res = $row->result();

  return $res;
}
public function upMatakuliah()
{
  $data = array(
    'nama_matkul' => $this->input->post('matkul'),
    'sks'         => $this->input->post('sks'),
    'semester'    => $this->input->post('sems'),
    'kode_jur'    => $this->input->post('jur'),
    'description' => $this->input->post('desc'),
    'referensi'   => $this->input->post('ref')

  );
  $this->db->where('kode_matkul',$this->input->post('komatkul'));
  $this->db->update('tb_matakuliah', $data);

  if (condition==TRUE) {
    redirect('matakuliah/index');
  }
}
}

/* End of file Matakuliah_model.php */
/* Location: ./application/models/Matakuliah_model.php */
