<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa_model extends CI_Model {

  public $variable;

  public function __construct()
  {
    parent::__construct();

  }
  function addMahas()
  {
    $data = array(
      'NIK' => $this->input->post('nim'),
      'nama_sis' => $this->input->post('nama'),
      'tem_lahir' => $this->input->post('tm_lahir'),
      'tgl_lahir' => $this->input->post('tg_lahir'),
      'j_kel'     => $this->input->post('jk'),
      'kode_jur'  => $this->input->post('jur'),
      'agama'     => $this->input->post('agama'),
      'alamat_sis'=> $this->input->post('alm'),
      'telpon'    => $this->input->post('telp'),
      'email'     => $this->input->post('email')
    );
    $this->db->insert('tb_mahasiswa', $data);

    if (condition==TRUE) {
      redirect('mahasiswa/index');
    }

    if($this->db->affected_rows() > 0)
      return True;
    else
      return False;
  }
  function tambahJur()
    {
      $this->db->select('*')
            ->from('tb_jurusan');
      $result=array();
      $query=$this->db->get();

      if ($query->num_rows() > 0) {
        $result= $query->result();
      }
      return $result;
    }
    function getEdit($kodeMaha)
    {
      $this->db->select('*')
          ->from('tb_mahasiswa')
          ->where('NIK',$kodeMaha);

          $row = $this->db->get();
          $res = $row->result();

          return $res;

    }
    function getmahasiswa1()
    {
      $this->db->select('m.NIK, m.nama_sis, m.j_kel, j.nama_jurusan,m.alamat_sis,m.email,m.telpon', FALSE)
          ->from('tb_mahasiswa m')
          ->join('tb_jurusan j','j.kode_jur=m.kode_jur');

          $result=array();
          $query=$this->db->get();

          if ($query->num_rows() > 0) {
            $result= $query->result();
          }
          return $result;
    }
    function upMahasiswa()
    {
      $data = array(
        'nama_sis'    =>$this->input->post('nama') ,
        'tem_lahir'   =>$this->input->post('tm_lahir'),
        'tgl_lahir'   =>$this->input->post('tg_lahir'),
        'j_kel'       =>$this->input->post('jk'),
        'kode_jur'    =>$this->input->post('jur'),
        'agama'       =>$this->input->post('agama'),
        'alamat_sis'  =>$this->input->post('alm'),
        'email'       =>$this->input->post('email'),
        'telpon'      =>$this->input->post('telp')
      );
      $this->db->where('NIK',$this->input->post('nim'));
      $this->db->update('tb_mahasiswa',$data);

      if (condition==TRUE) {
        redirect('home/mahasiswa');
      }
    }
}

/* End of file Mahasiswa_model.php */
/* Location: ./application/models/Mahasiswa_model.php */
