<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dosen_model extends CI_Model {

  public $variable;

  public function __construct()
  {
    parent::__construct();

  }
  function inDos()
  {

    $data = array(
      'NIDN' => $this->input->post('nidn') ,
      'nama_dos' => $this->input->post('nDos'),
      'j_kel' => $this->input->post('jk'),
      'jurusan'=> $this->input->post('jur'),
      'pendidikan' => $this->input->post('pen'),
      'telepon' => $this->input->post('telp'),
      'email' => $this->input->post('email'),
      'alamat' => $this->input->post('alm')
    );
    $this->db->insert('tb_dosen',$data);

    if (condition==TRUE) {
      redirect('home/dosen');
    }
  }
  function view_Dos()
  {
    $this->db->select('*')
          ->from('tb_dosen');
    $result=array();

    $query=$this->db->get();

    if ($query->num_rows() > 0) {
      $result=$query->result();
    }
    return $result;
  }
  function getDos($dos)
  {
    $this->db->select('*')
        ->from('tb_dosen')
        ->where('NIDN',$dos);
    $query = $this->db->get();

    return $query->result();
  }
  function UpDos()
  {
    $data=array(
      'nama_dos' => $this->input->post('nDos'),
      'j_kel' => $this->input->post('jk'),
      'jurusan'=> $this->input->post('jur'),
      'pendidikan' => $this->input->post('pen'),
      'telepon' => $this->input->post('telp'),
      'email' => $this->input->post('email'),
      'alamat' => $this->input->post('alm')
    );
    $this->db->where('NIDN',$this->input->post('nidn'));
    $this->db->update('tb_dosen',$data);

    if (condition==TRUE) {
      redirect('dos/dosen/viewDos');
    }
  }
}

/* End of file Dosen_model.php */
/* Location: ./application/models/Dosen_model.php */
