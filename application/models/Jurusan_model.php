<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurusan_model extends CI_Model {

  public function __construct()
  {
    parent::__construct();

  }

  function addjur()
  {
    $data = array(
      'kode_jur' => $this->input->post('kJur'),
      'nama_jurusan' => $this->input->post('nJur'),
      'kaprodi'=> $this->input->post('kaprodi'),
      'deskrip' => $this->input->post('desk')
    );
    $this->db->insert('tb_jurusan',$data);

    if (condition == TRUE) {
     redirect('jur/jurusan/index');
    }

    if($this->db->affected_rows() > 0)
      return True;
    else
      return False;
  }
  function viewJur()
  {
    $this->db->select('*')->from('tb_jurusan');
    $result=array();
    $query=$this->db->get();

    if ($query->num_rows() > 0) {
      $result=$query->result();
    }
    return $result;
  }
  function getJur($kodejur)
  {
    $this->db->select('*')
        ->from('tb_jurusan')
        ->where('kode_jur',$kodejur);
    $query=$this->db->get();

    return $query->result();
  }
  function upJur()
  {
    $data=array(
      'nama_jurusan'=>$this->input->post('nJur'),
      'kaprodi'=>$this->input->post('kaprodi'),
      'deskrip'=>$this->input->post('desk')
    );
    $this->db->where('kode_jur',$this->input->post('kJur'));
    $this->db->update('tb_jurusan',$data);


    if (condition == true) {
      redirect('jur/jurusan/index');
    }
  }
}

/* End of file Jurusan.php */
/* Location: ./application/models/Jurusan.php */
