<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jadwal extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {

    $data['menu'] =('templates/menu');
		$data['content'] =('jadwal/insert_jadwal');
		$this->load->view('templates/mainpage',$data);
  }

}

/* End of file Jadwal.php */
/* Location: ./application/controllers/Jadwal.php */
