<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dosen extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $data['menu'] = ('templates/menu');
    $data['content']=('dosen/dosen_input');
    $this->load->view('templates/mainpage',$data);
  }
  function saveDos()
  {
    $this->load->model('dosen_model');
    $this->dosen_model->inDos();
  }
  function viewDos()
  {
    $this->load->model('dosen_model');
    $data['menu'] = ('templates/menu');
    $data['list_dos'] = $this->dosen_model->view_Dos();
    $data['content']=('dosen/dosen_view');
    $this->load->view('templates/mainpage',$data);
  }
  function editDos()
  {
    $this->load->model('dosen_model');
    $data['menu'] = ('templates/menu');
    $data['get_Dos'] = $this->dosen_model->getDos($this->uri->segment('4'));
    $data['content'] = ('dosen/dosen_edit');
    $this->load->view('templates/mainpage',$data);
  }
  function edit()
  {
    $this->load->model('dosen_model');
    $this->dosen_model->upDos();
  }
  function delDos()
  {
    $this->db->where('NIDN',$this->uri->segment('4'));
    $this->db->delete('tb_dosen');

    redirect('dos/dosen/viewDos');
  }
}

/* End of file Dosen.php */
/* Location: ./application/controllers/Dosen.php */
