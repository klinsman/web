<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nilai extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
        $data['menu'] =('templates/menu');
    		$data['content'] =('nilai/insert_nilai');
    		$this->load->view('templates/mainpage',$data);
  }

}

/* End of file Nilai.php */
/* Location: ./application/controllers/Nilai.php */
