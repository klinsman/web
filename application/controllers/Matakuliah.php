<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Matakuliah extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->model('matakuliah_model');
    $data['menu'] = ('templates/menu');
    $data['content'] = ('matakuliah/matakuliah_view');
    $data['listMatakuliah'] = $this->matakuliah_model->getMatakuliah();
    $this->load->view('templates/mainpage',$data);
  }
  public function tambahMatakuliah($value='')
  {
    $this->load->model('matakuliah_model');
    $data['menu'] = ('templates/menu');
    $data['content'] = ('matakuliah/matakuliah_insert');
    $data['list_jur'] = $this->matakuliah_model->tambahJur();
    $this->load->view('templates/mainpage',$data);
  }
  public function saveMatakuliah()
  {
    $this->load->model('matakuliah_model');
    $this->matakuliah_model->saveMatakuliah();
  }
  public function delMatakuliah()
  {
    $this->db->where('kode_matkul',$this->uri->segment('3'));
    $this->db->delete('tb_matakuliah');

    redirect('matakuliah/index');
  }
  public function edit()
  {
    $this->load->model('matakuliah_model');
    $data['menu'] = ('templates/menu');
    $data['listMatakuliah'] = $this->matakuliah_model->getMatakuliah1($this->uri->segment('3'));
    $data['list_jur'] = $this->matakuliah_model->tambahJur();
    $data['content'] = ('matakuliah/matakuliah_edit');
    $this->load->view('templates/mainpage',$data);
  }
  public function Upmatakuliah()
  {
    $this->load->model('matakuliah_model');
    $this->matakuliah_model->Upmatakuliah();
  }
}

/* End of file Matakuliah.php */
/* Location: ./application/controllers/Matakuliah.php */
