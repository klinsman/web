/**
  *  COMMENT 
  */
	
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
  }

  public function index()
  {
    $this->load->model('mahasiswa_model');
    $data['menu'] =('templates/menu');
		$data['content'] =('mahasiswa/mahasiswa_insert');
    $data['list_jur'] = $this->mahasiswa_model->tambahJur();
		$this->load->view('templates/mainpage',$data);
  }
  function saveMahasiswa()
  {
    $this->load->model('mahasiswa_model');
    $this->mahasiswa_model->addMahas();
  }
  function edit()
  {
    $this->load->model('mahasiswa_model');
    $data['menu'] =('templates/menu');
		$data['content'] =('mahasiswa/mahasiswa_edit');
    $data['listMahasiswa']=$this->mahasiswa_model->getEdit($this->uri->segment('3'));
    $data['list_jur'] = $this->mahasiswa_model->tambahJur();
		$this->load->view('templates/mainpage',$data);
  }
  function updateMahasiswa()
  {
    $this->load->model('mahasiswa_model');
    $this->mahasiswa_model->upMahasiswa();
  }
  function delMahasiswa()
  {
    $this->db->where('NIK',$this->uri->segment('3'));
    $this->db->delete('tb_mahasiswa');

    redirect('home/mahasiswa');/**
      *   
      */
  }
	function coba(){
		$def = "coba";
	}
}

/* End of file Mahasiswa.php */
/* Location: ./application/controllers Mahasiswa.php */
