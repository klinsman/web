<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jurusan extends CI_Controller {

  function index()
  {
    $this->load->model('jurusan_model');
    $data['menu'] = ('templates/menu');
    $data['content'] = ('jurusan/view_jurusan');
    $data['list_jur']= $this->jurusan_model->viewJur();
    $this->load->view('templates/mainpage',$data);
  }
  function jur()
  {
    $data['menu'] = ('templates/menu');
    $data['content'] = ('jurusan/input_jurusan');
    $this->load->view('templates/mainpage',$data);
  }
  function addJur()
  {
    $this->load->model('jurusan_model');
    $this->jurusan_model->addjur();
  }
  function editJur()
  {
    $this->load->model('jurusan_model');
    $data['menu']=('templates/menu');
    $data['content']=('jurusan/edit_jurusan');
    $data['list_jur']=$this->jurusan_model->getJur($this->uri->segment('4'));
    $this->load->view('templates/mainpage',$data);
  }
  public function edit()
  {
    $this->load->model('jurusan_model');
    $this->jurusan_model->upJur();
  }
  function delJur()
  {
    $this->db->where('kode_jur',$this->uri->segment('4'));
    $this->db->delete('tb_jurusan');

    redirect('jur/jurusan/index');
  }
}
